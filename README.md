# Telegram S3 Bot

Telegram bot for AWS S3 bucket management.

## docker compose

```yml
---
version: "3"
services:
  telegram-s3-bot:
    image: registry.gitlab.com/eh-san/telegram-s3-bot:latest
    container_name: telegram-s3-bot
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
      - TELEGRAM_API_TOKEN=${TELEGRAM_API_TOKEN}
      - TELEGRAM_USERNAME=${TELEGRAM_USERNAME}
      - AWS_SERVER_PUBLIC_KEY=${AWS_SERVER_PUBLIC_KEY}
      - AWS_SERVER_SECRET_KEY=${AWS_SERVER_SECRET_KEY}
      - AWS_REGION=${AWS_REGION}
      - ENDPOINT_URL=${ENDPOINT_URL}
      - EDGE_ENDPOINT_URL=${EDGE_ENDPOINT_URL}
      - CUSTOM_ENDPOINT_URL=${CUSTOM_ENDPOINT_URL}
      - BUCKET_NAME=${BUCKET_NAME}
      - TEMP_PATH=${TEMP_PATH}
      - DIGITALOCEAN_TOKEN=${DIGITALOCEAN_TOKEN}
    volumes:
      - /<path-to-some-tmp>:/tmp
    restart: unless-stopped

```
